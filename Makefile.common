#  Software License Agreement (BSD License)
#  Copyright (c) 2003-2016, CHAI3D.
#  (www.chai3d.org)
#
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  * Redistributions of source code must retain the above copyright
#  notice, this list of conditions and the following disclaimer.
#
#  * Redistributions in binary form must reproduce the above
#  copyright notice, this list of conditions and the following
#  disclaimer in the documentation and/or other materials provided
#  with the distribution.
#
#  * Neither the name of CHAI3D nor the names of its contributors may
#  be used to endorse or promote products derived from this software
#  without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
#
#  $Author: seb $
#  $Date: 2016-01-21 16:13:27 +0100 (Thu, 21 Jan 2016) $
#  $Rev: 1906 $

##
#
# Following Makefile modified for QNX based on Force Dimensions SDK QNX Setup
#
##

# config settings
include $(TOP_DIR)/Makefile.config

# determine build configuration
ifneq ($(CFG),debug)
CFG = release
endif

# target library name
LIB_NAME = chai3d

# QNX platform
ARCH   ?= x86_64
OS 		= qnx


# compiler
CC		 = qcc
CXX		 = q++
CMPVER   = $(shell echo `$(CC) -V 2>&1  | sed -n '/,/{s/,.*//;s/^[ \t]*//;p;q}'`)
COMPILER = qcc-$(CMPVER)

# common paths
BIN_DIR = $(TOP_DIR)/bin/$(OS)-$(ARCH)
LIB_DIR = $(TOP_DIR)/lib/$(CFG)/$(OS)-$(ARCH)-$(COMPILER)

# common compiler flags
CXXFLAGS += -I$(TOP_DIR)/src -fsigned-char -DQNX -V$(CMPVER),gcc_nto$(ARCH)

# build configuration specific flags
ifneq ($(CFG),debug)
CXXFLAGS += -O3
else
CXXFLAGS += -O0 -g -DDEBUG
endif

# common librarian flags
AR = nto$(subst be,,$(subst le,,$(ARCH)))-ar
ARFLAGS = rs

# target library
LIB_TARGET = $(LIB_DIR)/lib$(LIB_NAME).a

# common linker flags
LDFLAGS = -L$(LIB_DIR)
LDLIBS  = -l$(LIB_NAME)

# optional linker flags (see Makefile.config)
# ifeq ($(USE_EXTERNAL_LIBPNG), yes)
# $LDLIBS += -lpng -lz
# $endif
# $ifeq ($(USE_EXTERNAL_LIBJPEG), yes)
# $LDLIBS += -ljpeg
# $endif
# $ifeq ($(USE_EXTERNAL_GIFLIB), yes)
# $LDLIBS += -lgif
# $endif
# $ifeq ($(USE_EXTERNAL_LIB3DS), yes)
# $LDLIBS += -l3ds
# $endif
ifeq ($(USE_EXTERNAL_OPENAL), yes)
LDLIBS += -lopenal
endif
# $ifeq ($(USE_EXTERNAL_THEORAPLAYER), yes)
# $LDLIBS += -ltheoraplayer
# $endif

# Eigen dependency
# EIGEN     = $(TOP_DIR)/external/Eigen
# CXXFLAGS += -I$(EIGEN)

# GLEW dependency
# GLEW_DIR = $(TOP_DIR)/external/glew
# CXXFLAGS += -I$(GLEW_DIR)/include

# DHD dependency
DHD_EXT  ?= $(TOP_DIR)/external/DHD
DHD_LIB  ?= $(DHD_EXT)/lib/$(OS)-$(ARCH)
CXXFLAGS += -I$(DHD_EXT)/include
LDFLAGS  += -L$(DHD_LIB)
LDLIBS   += -ldrd -lm -lusbdi -lsocket
